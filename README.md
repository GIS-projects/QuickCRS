# ![icon](https://gitlab.com/GIS-projects/QuickCRS/raw/master/QuickCRS/icon.png) 'QuickCRS' for [QGIS 3.x](http://qgis.org)
*A QGIS plugin to set the CRS of the current project to your favourite CRS with just one click.*

It's very easy to set your default CRS this plugin uses in the Plugin Settings Menu.

** If you discover a bug, please [report it here](https://gitlab.com/GIS-projects/QuickCRS/issues).**