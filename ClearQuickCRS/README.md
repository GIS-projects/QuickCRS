# ClearQuickCRS: QGIS plugin used during QuickCRS development
It removes the `quickcrs/crs` setting, so QuickCRS can be tested as if it's just installed.